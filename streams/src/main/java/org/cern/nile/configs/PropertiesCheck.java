package org.cern.nile.configs;

import javax.validation.constraints.NotNull;
import java.util.Properties;
import java.util.Set;

public class PropertiesCheck {

  private final Set<String> clientProperties = StreamConfig.ClientProperties.getConfigs();
  private final Set<String> commonProperties = StreamConfig.CommonProperties.getConfigs();
  private final Set<String> decodingProperties = StreamConfig.DecodingProperties.getConfigs();
  private final Set<String> routingProperties = StreamConfig.RoutingProperties.getConfigs();
  private final Set<String> enrichmentProperties = StreamConfig.EnrichmentProperties.getConfigs();

  public PropertiesCheck() {
  }

  /**
   * Validates the properties file based on the type of stream.
   *
   * @param streamType - type of stream defined in the properties file.
   * @param properties - properties already loaded from file into java.util.Properties object.
   */
  public void validateProperties(@NotNull Properties properties, @NotNull StreamType streamType) {
    if (properties == null) {
      throw new RuntimeException("Properties object cannot be null");
    }

    if (streamType == null) {
      throw new RuntimeException("Properties file is missing stream.type property");
    }

    // Check for client props
    this.cycleProps(properties, this.clientProperties);

    // Check for common props
    this.cycleProps(properties, this.commonProperties);

    switch (streamType) {
      case DECODING:
        this.cycleProps(properties, this.decodingProperties);
        break;
      case ROUTING:
        this.cycleProps(properties, this.routingProperties);
        break;
      case ENRICHMENT:
        this.cycleProps(properties, this.enrichmentProperties);
        break;
      default:
        throw new RuntimeException("Stream type unknown");
    }
  }

  private void cycleProps(@NotNull Properties props, @NotNull Set<String> propsToCheck) {
    if (props == null) {
      throw new RuntimeException("Properties object cannot be null");
    }

    for (String prop : propsToCheck) {
      if (!props.containsKey(prop)) {
        throw new RuntimeException("Properties file is missing: " + prop + " property.");
      }
    }
  }
}
