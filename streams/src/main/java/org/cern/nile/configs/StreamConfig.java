package org.cern.nile.configs;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamConfig {

  public enum ClientProperties {
    SOURCE_TOPIC("source.topic"),
    KAFKA_CLUSTER("kafka.cluster"),
    CLIENT_ID("client.id"),
    TRUSTSTORE_LOCATION("truststore.location");

    private String value;

    ClientProperties(String value) {
      this.value = value;
    }

    public static ClientProperties fromString(String value) throws IllegalArgumentException {
      return Arrays.stream(ClientProperties.values()).filter(p -> p.value.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public static Set<String> getConfigs() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }

  public enum CommonProperties {
    STREAM_TYPE("stream.type"),
    STREAM_CLASS("stream.class");

    private String value;

    CommonProperties(String value) {
      this.value = value;
    }

    public static CommonProperties fromString(String value) throws IllegalArgumentException {
      return Arrays.stream(CommonProperties.values()).filter(p -> p.value.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public static Set<String> getConfigs() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }


  public enum DecodingProperties {
    SINK_TOPIC("sink.topic");

    private String value;

    DecodingProperties(String value) {
      this.value = value;
    }

    public static DecodingProperties fromString(String value) throws IllegalArgumentException {
      return Arrays.stream(DecodingProperties.values()).filter(p -> p.value.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public static Set<String> getConfigs() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }

  public enum RoutingProperties {
    ROUTING_CONFIG_PATH("routing.config.path");

    private String value;

    RoutingProperties(String value) {
      this.value = value;
    }

    public static RoutingProperties fromString(String value) throws IllegalArgumentException {
      return Arrays.stream(RoutingProperties.values()).filter(p -> p.value.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public static Set<String> getConfigs() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }

  public enum EnrichmentProperties {
    ENRICHMENT_CONFIG_PATH("enrichment.config.path"),
    SINK_TOPIC("sink.topic");

    private String value;

    EnrichmentProperties(String value) {
      this.value = value;
    }

    public static EnrichmentProperties fromString(String value) throws IllegalArgumentException {
      return Arrays.stream(EnrichmentProperties.values()).filter(p -> p.value.equals(value)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public static Set<String> getConfigs() {
      return Arrays.stream(values()).map(o -> o.value).collect(Collectors.toSet());
    }

    public String getValue() {
      return value;
    }
  }
}
