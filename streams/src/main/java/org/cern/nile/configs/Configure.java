package org.cern.nile.configs;

import java.util.Properties;

public interface Configure {
  void configure(Properties configs);
}
