package org.cern.nile.configs;

import java.text.MessageFormat;
import java.util.Arrays;

public enum StreamType {
  ROUTING("ROUTING"),
  DECODING("DECODING"),
  ENRICHMENT("ENRICHMENT");

  private String value;

  StreamType(String value) {
    this.value = value;
  }

  /**
   * Finds the Stream type.
   *
   * @param type - string of type. defined in the configuration of the application.
   * @return StreamType if found otherwise throws RuntimeException.
   */
  public static StreamType findByValue(final String type) {
    return Arrays.stream(values()).filter(streamType -> streamType.value.equals(type)).findFirst().orElseThrow(() ->
           new RuntimeException(MessageFormat.format("Could not find the type of streaming. Please add the property {0} followed by: {1} options",
                                                     StreamConfig.CommonProperties.STREAM_TYPE.getValue(), Arrays.asList(StreamType.values()))));
  }
}
