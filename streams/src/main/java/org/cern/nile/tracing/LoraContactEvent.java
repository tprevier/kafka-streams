package org.cern.nile.tracing;

public class LoraContactEvent {
  private final Object rfTagId;
  private final Object timestamp;
  private final Object contact;

  /**
   * Object holder.
   *
   * @param rfTagId      - id from first device
   * @param timestamp - timestamp from the encounter
   * @param contact      - information about the encountered device
   */
  public LoraContactEvent(Object rfTagId, Object timestamp, Object contact) {
    this.rfTagId = rfTagId;
    this.timestamp = timestamp;
    this.contact = contact;
  }

  public Object getRfTagId() {
    return rfTagId;
  }

  public Object getTimestamp() {
    return timestamp;
  }

  public Object getContact() {
    return contact;
  }

}
