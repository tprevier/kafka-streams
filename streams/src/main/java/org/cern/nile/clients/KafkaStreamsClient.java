package org.cern.nile.clients;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.errors.DefaultProductionExceptionHandler;
import org.apache.kafka.streams.errors.LogAndContinueExceptionHandler;
import org.cern.nile.configs.Configure;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

public class KafkaStreamsClient implements Configure {

  private Properties properties;

  /**
   * Default constructor for Kafka Streams Client.
   */
  public KafkaStreamsClient() {
  }

  @Override
  public void configure(Properties configs) {
    final String lora_id = configs.getProperty(StreamConfig.ClientProperties.CLIENT_ID.getValue());
    properties = new Properties();
    properties.put(StreamsConfig.APPLICATION_ID_CONFIG, lora_id);
    properties.put(StreamsConfig.CLIENT_ID_CONFIG, lora_id);
    properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,
                   this.revDnsCluster(configs.getProperty(StreamConfig.ClientProperties.KAFKA_CLUSTER.getValue())));
    properties.put(StreamsConfig.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
    properties.put(SaslConfigs.SASL_MECHANISM, "GSSAPI");
    properties.put(SaslConfigs.SASL_KERBEROS_SERVICE_NAME, "kafka");
    properties.put(SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, configs.getProperty(StreamConfig.ClientProperties.TRUSTSTORE_LOCATION.getValue()));
    properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
    properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, JsonSerde.class.getName());
    properties.put(StreamsConfig.DEFAULT_DESERIALIZATION_EXCEPTION_HANDLER_CLASS_CONFIG, LogAndContinueExceptionHandler.class.getName());
    properties.put(StreamsConfig.DEFAULT_PRODUCTION_EXCEPTION_HANDLER_CLASS_CONFIG, DefaultProductionExceptionHandler.class.getName());
  }

  public KafkaStreams create(Topology topology) {
    return new KafkaStreams(topology, properties);
  }

  private String revDnsCluster(String kafkaCluster) {
    try {
      StringBuilder sb = new StringBuilder();
      InetAddress[] address = InetAddress.getAllByName(kafkaCluster);
      for (InetAddress host : address) {
        final String hostName = InetAddress.getByName(host.getHostAddress()).getHostName();
        sb.append(hostName).append(":9093,");
      }
      sb.deleteCharAt(sb.length() - 1);
      return sb.toString();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }
}
