package org.cern.nile;

import org.cern.nile.models.Device;

import java.util.Collection;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class Utils {

  /**
   * Gets the first object that is found provided that it matches the pattern.
   *
   * @param devTopic - Collection of devices that contain the topics
   * @param devId    - The String-id of the device
   * @return - Optional of org.cern.nile.models.Device
   * @throws IllegalArgumentException If the expression's syntax is invalid
   */
  public static Optional<Device> getFirst(Collection<Device> devTopic, String devId) throws IllegalArgumentException {
    return devTopic.stream().filter(d -> {
      try {
        return Pattern.compile(d.getName()).matcher(devId).matches();
      } catch (PatternSyntaxException e) {
        throw new IllegalArgumentException(e);
      }
    }).findFirst();
  }
}
