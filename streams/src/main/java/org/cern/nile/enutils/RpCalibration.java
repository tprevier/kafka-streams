package org.cern.nile.enutils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RpCalibration {
  private static final Logger LOGGER = LoggerFactory.getLogger(RpCalibration.class.getName());
  private static final double VOLTAGE_CONSTANT = 0.0008056640625;
  private static final double VOLTAGE_CONSTANT2 = 0.00185302734375;

  /**
   * Decodes payload and adds properties to the returning objects.
   *
   * @param message - incoming message
   * @return - Map of properties of the decoded payload
   */
  public static Collection<Map<String, Object>> decode(JsonObject message)
      throws IOException, ParseException, NullPointerException {
    // Payload vars
    final String payloadRaw = message.get("data").getAsString();
    final byte[] payloadDecoded = Base64.getDecoder().decode(payloadRaw);
    final DataInputStream inputStream = new DataInputStream(new ByteArrayInputStream(payloadDecoded));

    final Collection<Map<String, Object>> output = new ArrayList<>();
    if (inputStream.available() > 0) {
      output.add(getSpecificPayloadData(decodePayload(inputStream), message));
    }
    return output;
  }

  private static Map<String, Object> getSpecificPayloadData(Map<String, Object> payloadData, JsonObject message)
      throws ParseException, NullPointerException {
    final Map<String, Object> toReturn = new HashMap<>();
    toReturn.put("dev_ID", payloadData.get("dev_ID"));
    toReturn.put("package_num", payloadData.get("package_num"));
    toReturn.put("alarm", payloadData.get("alarm"));
    toReturn.put("counts", payloadData.get("counts"));
    toReturn.put("checking_time", payloadData.get("checkingTime"));
    toReturn.put("shock_counts", payloadData.get("shockCounts"));
    toReturn.put("alarm_counts", payloadData.get("alarm_counts"));
    toReturn.put("temperature", payloadData.get("temperature"));
    toReturn.put("timestamp", System.currentTimeMillis());
    toReturn.put("mon3v3", payloadData.get("mon3v3"));
    toReturn.put("mon3v3_voltage", payloadData.get("mon3v3_voltage"));
    toReturn.put("mon5", payloadData.get("mon5"));
    toReturn.put("mon5_voltage", payloadData.get("mon5_voltage"));
    toReturn.put("monVin", payloadData.get("monVin"));
    toReturn.put("monVin_voltage", payloadData.get("monVin_voltage"));
    toReturn.put("exwdtc", payloadData.get("exwdtc"));
    appendMetadata(message, toReturn);
    return toReturn;
  }

  private static Map<String, Object> decodePayload(DataInputStream inputStream) throws IOException {
    final Map<String, Object> toReturn = new HashMap<>();
    // Common bytes
    decodeCommonFirst(inputStream, toReturn);
    //30 bytes of sensor payload data
    final int alarm = inputStream.readUnsignedByte();
    final int alarmCounts = inputStream.readInt();
    final int counts = inputStream.readInt();
    final int counts1hAgo = inputStream.readInt();
    final int counts2hAgo = inputStream.readInt();
    final int checkingTime = inputStream.readUnsignedShort();
    final int checkingTime1hAgo = inputStream.readUnsignedShort();
    final int checkingTime2hAgo = inputStream.readUnsignedShort();
    final int shockCounts = inputStream.readInt();
    final int temperature = inputStream.readUnsignedShort();
    inputStream.skipBytes(1); // unused
    toReturn.put("alarm", alarm);
    toReturn.put("alarm_counts", alarmCounts);
    toReturn.put("counts", counts);
    toReturn.put("counts1hAgo", counts1hAgo);
    toReturn.put("counts2hAgo", counts2hAgo);
    toReturn.put("checkingTime", checkingTime);
    toReturn.put("checkingTime1hAgo", checkingTime1hAgo);
    toReturn.put("checkingTime2hAgo", checkingTime2hAgo);
    toReturn.put("shockCounts", shockCounts);
    toReturn.put("temperature", temperature);
    // Voltages and extWDTcounter
    decodeCommonSecond(inputStream, toReturn);
    return toReturn;
  }

  private static void decodeCommonFirst(DataInputStream inputStream, Map<String, Object> obj) throws IOException {
    final int devId = inputStream.readUnsignedByte();
    final int pNum = inputStream.readUnsignedShort();
    obj.put("dev_ID", devId);
    obj.put("package_num", pNum);
  }

  private static void decodeCommonSecond(DataInputStream inputStream, Map<String, Object> obj) throws IOException {
    final int mon3v3 = inputStream.readUnsignedShort();
    final int mon5 = inputStream.readUnsignedShort();
    final int monVin = inputStream.readUnsignedShort();
    final int exwdtc = inputStream.readUnsignedShort();
    final double mon3v3_voltage = mon3v3 * VOLTAGE_CONSTANT;
    final double mon5_voltage = mon5 * VOLTAGE_CONSTANT;
    final double monVin_voltage = monVin * VOLTAGE_CONSTANT2;
    obj.put("mon3v3", mon3v3);
    obj.put("mon3v3_voltage", mon3v3_voltage);
    obj.put("mon5", mon5);
    obj.put("mon5_voltage", mon5_voltage);
    obj.put("monVin", monVin);
    obj.put("monVin_voltage", monVin_voltage);
    obj.put("exwdtc", exwdtc);
  }

  private static void appendMetadata(JsonObject message, Map<String, Object> obj) throws ParseException, NullPointerException {
    // Adding device name
    final String devName = message.get("deviceName").getAsString();
    obj.put("device_name", devName);
    final JsonArray metadata = message.get("rxInfo").getAsJsonArray();
    // Adding timestamp
    boolean timestampFound = false;
    for (int i = 0; i < metadata.size(); i++) {
      final JsonObject entry = metadata.get(i).getAsJsonObject();
      if (entry.get("time") != null) {
        final long timestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(entry.get("time").getAsString()).getTime();
        obj.put("timestamp", timestamp);
        timestampFound = true;
        break;
      }
    }
    if (!timestampFound) {
      throw new NullPointerException("Could not find timestamp of the message in any of the gateways info. Dropping message: " + message.toString());
    }
    // Adding port as property
    obj.put("fPort", message.get("fPort").getAsString());
    // Adding rssi(s)/snr(s) as property(ies)
    for (int i = 0; i < metadata.size(); i++) {
      final JsonObject entry = metadata.get(i).getAsJsonObject();
      String objToSearch = "rssi";
      final JsonElement rssi = entry.get(objToSearch);
      if (rssi != null) {
        obj.put(String.format("rssi_gw_%d", i), rssi.getAsInt());
      } else {
        LOGGER.warn(String.format("Gateway: %d did not contain RSSI. Not appending value", i));
      }
      objToSearch = "loRaSNR";
      final JsonElement snr = entry.get(objToSearch);
      if (snr != null) {
        obj.put(String.format("snr_gw_%d", i), snr.getAsInt());
      } else {
        LOGGER.warn(String.format("Gateway: %d did not contain SNR. Not appending value", i));
      }
    }
  }
}
