package org.cern.nile.enutils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Decoder {
  private static final Logger LOGGER = LoggerFactory.getLogger(Decoder.class.getName());

  /**
   * Decodes payload based on the input port.
   *
   * @param port        - port coming from the payload
   * @param inputStream - input of the payload_raw after b64 decoding
   * @param metadata    - metadata of LoRa device
   * @param devId       - device id from the metadata of LoRa device
   * @return - Map of properties of the decoded payload
   */
  public static Map decode(int port, DataInputStream inputStream, JsonArray metadata, String devId) throws IOException, ParseException,
      RuntimeException {
    final String time = metadata.get(0).getAsJsonObject().get("time").getAsString();
    if (time == null) {
      throw new RuntimeException("Timestamp was not found");
    }
    switch (port) {
      case 0x01:
        return decodePort1(inputStream, time, devId, metadata);
      case 0xcc:
        return decodePort204(inputStream, time, devId, metadata);
      case 0x03:
        return decodePort3(inputStream, time, devId, metadata);
      default:
        throw new RuntimeException("Decoding port not known: " + port);
    }
  }

  private static Map decodePort3(DataInputStream inputStream, String time, String devId, JsonArray metadata) throws IOException, ParseException {
    final Map<String, Object> toReturn = new HashMap<>();
    while (inputStream.available() > 0) {
      // Common bytes
      decodeCommonFirst(inputStream, toReturn);
      // Specific port
      final int count_th1 = inputStream.readInt();
      final int count_th2 = inputStream.readInt();
      final int count_shocks = inputStream.readInt();
      final int checking_time = inputStream.readUnsignedShort();
      final int alarm = inputStream.readUnsignedByte();
      inputStream.skipBytes(15); // unused

      // 03-09-2020 Changed voltage constant. Stopped from being common to the other ports.
      final double voltageConstant = (0.000805664); // (1.65 * 2 / 4096)
      final double voltageConstant2 = (0.001853027); // (1.65 * (46/10) / 4096)
      final int mon3v3 = inputStream.readUnsignedShort();
      final int mon5 = inputStream.readUnsignedShort();
      final int monVin = inputStream.readUnsignedShort();
      final int exwdtc = inputStream.readUnsignedShort();
      final double mon3v3_voltage = mon3v3 * voltageConstant;
      final double mon5_voltage = mon5 * voltageConstant;
      final double monVin_voltage = monVin * voltageConstant2;
      toReturn.put("mon3v3", mon3v3);
      toReturn.put("mon3v3_voltage", mon3v3_voltage);
      toReturn.put("mon5", mon5);
      toReturn.put("mon5_voltage", mon5_voltage);
      toReturn.put("monVin", monVin);
      toReturn.put("monVin_voltage", monVin_voltage);
      toReturn.put("exwdtc", exwdtc);
      // 03-09-2020

      toReturn.put("count_th1", count_th1);
      toReturn.put("count_th2", count_th2);
      toReturn.put("count_shocks", count_shocks);
      toReturn.put("checking_time", checking_time);
      toReturn.put("alarm", alarm);
      toReturn.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time).getTime());
      toReturn.put("devID", devId);
      appendMetadata(metadata, toReturn);
    }
    return toReturn;
  }

  private static Map decodePort204(DataInputStream inputStream, String time, String devId, JsonArray metadata) throws IOException, ParseException {
    final Map<String, Object> toReturn = new HashMap<>();
    while (inputStream.available() > 0) {
      // Common bytes
      decodeCommonFirst(inputStream, toReturn);
      // Specific port
      final int dr = inputStream.readUnsignedByte();
      final int txPow = inputStream.readUnsignedByte();
      final int second = inputStream.readUnsignedByte();
      final int minute = inputStream.readUnsignedByte();
      final int hour = inputStream.readUnsignedByte();
      inputStream.skipBytes(1); // unused
      final int day = inputStream.readUnsignedByte();
      final int month = inputStream.readUnsignedByte();
      // Need to shift because it is short
      final int year = Integer.reverseBytes(inputStream.readUnsignedShort()) >> 16;
      final int adrAckReq = inputStream.readByte();
      inputStream.skipBytes(1); // unused
      final int msAwake = Integer.reverseBytes(inputStream.readInt());
      inputStream.skipBytes(14); // unused
      // Common bytes
      decodeCommonSecond(inputStream, toReturn);
      toReturn.put("dr", dr);
      toReturn.put("txPow", txPow);
      toReturn.put("adrAckReq", adrAckReq);
      toReturn.put("msAwake", msAwake);
      toReturn.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time).getTime());
      toReturn.put("devID", devId);
      appendMetadata(metadata, toReturn);
    }
    return toReturn;
  }

  private static Map decodePort1(DataInputStream inputStream, String time, String devId, JsonArray metadata) throws IOException, ParseException {
    final Map<String, Object> toReturn = new HashMap<>();
    while (inputStream.available() > 0) {
      // Common bytes
      decodeCommonFirst(inputStream, toReturn);
      // Specific port
      final int fsens1 = new BigInteger(inputStream.readNBytes(3)).intValue();
      final int fref1 = new BigInteger(inputStream.readNBytes(3)).intValue();
      final int recharge1 = inputStream.readUnsignedByte();
      final int fsens2 = new BigInteger(inputStream.readNBytes(3)).intValue();
      final int fref2 = new BigInteger(inputStream.readNBytes(3)).intValue();
      final int recharge2 = inputStream.readUnsignedByte();
      final int seu0 = inputStream.readUnsignedShort();
      final int seu1 = inputStream.readUnsignedShort();
      final int seu2 = inputStream.readUnsignedShort();
      final int seu3 = inputStream.readUnsignedShort();
      final int seu4 = inputStream.readUnsignedShort();
      final int seu5 = inputStream.readUnsignedShort();
      final int seu6 = inputStream.readUnsignedShort();
      final int seu7 = inputStream.readUnsignedShort();
      // Common bytes
      decodeCommonSecond(inputStream, toReturn);
      toReturn.put("fsens1", fsens1);
      toReturn.put("fref1", fref1);
      toReturn.put("recharge1", recharge1);
      toReturn.put("fsens2", fsens2);
      toReturn.put("fref2", fref2);
      toReturn.put("recharge2", recharge2);
      toReturn.put("seu0", seu0);
      toReturn.put("seu1", seu1);
      toReturn.put("seu2", seu2);
      toReturn.put("seu3", seu3);
      toReturn.put("seu4", seu4);
      toReturn.put("seu5", seu5);
      toReturn.put("seu6", seu6);
      toReturn.put("seu7", seu7);
      toReturn.put("devID", devId);
      toReturn.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time).getTime());
      appendMetadata(metadata, toReturn);
    }
    return toReturn;
  }

  private static void decodeCommonFirst(DataInputStream inputStream, Map<String, Object> obj) throws IOException {
    final int devId = inputStream.readUnsignedByte();
    final int pNum = inputStream.readUnsignedShort();
    obj.put("devID_packet", devId);
    obj.put("pNum", pNum);
  }

  private static void decodeCommonSecond(DataInputStream inputStream, Map<String, Object> obj) throws IOException {
    final double voltageConstant = (0.001611328125); // (20 / 10 * 3.3 / 4096)
    final double voltageConstant2 = (0.00322265625); // (46 / 10 * 3.3 / 4096)
    final int mon3v3 = inputStream.readUnsignedShort();
    final int mon5 = inputStream.readUnsignedShort();
    final int monVin = inputStream.readUnsignedShort();
    final int exwdtc = inputStream.readUnsignedShort();
    final double mon3v3_voltage = mon3v3 * voltageConstant;
    final double mon5_voltage = mon5 * voltageConstant;
    final double monVin_voltage = monVin * voltageConstant2;
    obj.put("mon3v3", mon3v3);
    obj.put("mon3v3_voltage", mon3v3_voltage);
    obj.put("mon5", mon5);
    obj.put("mon5_voltage", mon5_voltage);
    obj.put("monVin", monVin);
    obj.put("monVin_voltage", monVin_voltage);
    obj.put("exwdtc", exwdtc);
  }

  private static void appendMetadata(JsonArray metadata, Map<String, Object> obj) {
    for (int i = 0; i < metadata.size(); i++) {
      final JsonObject entry = metadata.get(i).getAsJsonObject();
      String objToSearch = "rssi";
      final JsonElement rssi = entry.get(objToSearch);
      if (rssi != null) {
        obj.put(String.format("rssi_gw_%d", i), rssi.getAsInt());
      } else {
        LOGGER.warn(String.format("Gateway: %d did not contain RSSI. Not appending value", i));
      }
      objToSearch = "loRaSNR";
      final JsonElement snr = entry.get(objToSearch);
      if (snr != null) {
        obj.put(String.format("snr_gw_%d", i), snr.getAsInt());
      } else {
        LOGGER.warn(String.format("Gateway: %d did not contain SNR. Not appending value", i));
      }
    }
  }
}


