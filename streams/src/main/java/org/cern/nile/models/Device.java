package org.cern.nile.models;

public class Device {
  private String name;
  private Topic topic;

  public Device() {
  }

  public String getName() {
    return name;
  }

  public Topic getTopic() {
    return topic;
  }


  @Override
  public String toString() {
    return "Device{"
        + ", name='" + name + '\''
        + ", topic=" + topic
        + '}';
  }
}
