package org.cern.nile.models;

public class DevicePayload {
  private int deviceID;
  private int counter;
  private int accDose;
  private short lastHourDose;
  private short twoHourDose;
  private short threeHourDose;
  private short voltage;
  private short check;

  /**
   * Constructor.
   */
  public DevicePayload(
      int deviceID, int counter, int accDose, short lastHourDose, short twoHourDose, short threeHourDose, short voltage,
      short check) {
    this.deviceID = deviceID;
    this.counter = counter;
    this.accDose = accDose;
    this.lastHourDose = lastHourDose;
    this.twoHourDose = twoHourDose;
    this.threeHourDose = threeHourDose;
    this.voltage = voltage;
    this.check = check;
  }

  public int getDeviceID() {
    return deviceID;
  }

  public int getCounter() {
    return counter;
  }

  public int getAccDose() {
    return accDose;
  }

  public short getLastHourDose() {
    return lastHourDose;
  }

  public short getTwoHourDose() {
    return twoHourDose;
  }

  public short getThreeHourDose() {
    return threeHourDose;
  }

  public short getVoltage() {
    return voltage;
  }

  public short getCheck() {
    return check;
  }

  @Override
  public String toString() {
    return "DevicePayload{"
        + "deviceID=" + deviceID
        + ", counter=" + counter
        + ", accDose=" + accDose
        + ", lastHourDose=" + lastHourDose
        + ", twoHourDose=" + twoHourDose
        + ", threeHourDose=" + threeHourDose
        + ", voltage=" + voltage
        + ", check=" + check
        + '}';
  }
}
