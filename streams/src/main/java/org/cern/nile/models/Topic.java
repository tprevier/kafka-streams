package org.cern.nile.models;

public class Topic {
  private String name;

  public Topic() {
  }

  public String getName() {
    return name;
  }

  @Override
  public String toString() {
    return "Topic{"
        + "name='" + name + '\''
        + '}';
  }
}
