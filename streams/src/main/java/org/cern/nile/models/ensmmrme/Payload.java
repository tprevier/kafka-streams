package org.cern.nile.models.ensmmrme;

public class Payload {
  private final int deviceId;
  private final int pnum;
  private final int fsens1;
  private final int fref1;
  private final int recharge1;
  private final int fsens2;
  private final int fref2;
  private final int recharge2;
  private final int seu0;
  private final int seu1;
  private final int seu2;
  private final int seu3;
  private final int seu4;
  private final int seu5;
  private final int seu6;
  private final int seu7;
  private final int mon3v3;
  private final int mon5;
  private final int monVin;
  private final int exwdtc;

  /**
   * Public constructor to be used as a object holder for the payload coming from the EN-SMM-RME team.
   */
  public Payload(
      int deviceId, int pnum, int fsens1, int fref1, int recharge1, int fsens2, int fref2, int recharge2, int seu0, int seu1, int seu2,
      int seu3, int seu4, int seu5, int seu6, int seu7, int mon3v3, int mon5, int monVin, int exwdtc) {
    this.deviceId = deviceId;
    this.pnum = pnum;
    this.fsens1 = fsens1;
    this.fref1 = fref1;
    this.recharge1 = recharge1;
    this.fsens2 = fsens2;
    this.fref2 = fref2;
    this.recharge2 = recharge2;
    this.seu0 = seu0;
    this.seu1 = seu1;
    this.seu2 = seu2;
    this.seu3 = seu3;
    this.seu4 = seu4;
    this.seu5 = seu5;
    this.seu6 = seu6;
    this.seu7 = seu7;
    this.mon3v3 = mon3v3;
    this.mon5 = mon5;
    this.monVin = monVin;
    this.exwdtc = exwdtc;
  }

  public int getDeviceId() {
    return deviceId;
  }

  public int getPnum() {
    return pnum;
  }

  public int getFsens1() {
    return fsens1;
  }

  public int getFref1() {
    return fref1;
  }

  public int getRecharge1() {
    return recharge1;
  }

  public int getFsens2() {
    return fsens2;
  }

  public int getFref2() {
    return fref2;
  }

  public int getRecharge2() {
    return recharge2;
  }

  public int getSeu0() {
    return seu0;
  }

  public int getSeu1() {
    return seu1;
  }

  public int getSeu2() {
    return seu2;
  }

  public int getSeu3() {
    return seu3;
  }

  public int getSeu4() {
    return seu4;
  }

  public int getSeu5() {
    return seu5;
  }

  public int getSeu6() {
    return seu6;
  }

  public int getSeu7() {
    return seu7;
  }

  public int getMon3v3() {
    return mon3v3;
  }

  public int getMon5() {
    return mon5;
  }

  public int getMonVin() {
    return monVin;
  }

  public int getExwdtc() {
    return exwdtc;
  }

  @Override
  public String toString() {
    return "Payload{"
        + "deviceId=" + deviceId
        + ", pnum=" + pnum
        + ", fsens1=" + fsens1
        + ", fref1=" + fref1
        + ", recharge1=" + recharge1
        + ", fsens2=" + fsens2
        + ", fref2=" + fref2
        + ", recharge2=" + recharge2
        + ", seu0=" + seu0
        + ", seu1=" + seu1
        + ", seu2=" + seu2
        + ", seu3=" + seu3
        + ", seu4=" + seu4
        + ", seu5=" + seu5
        + ", seu6=" + seu6
        + ", seu7=" + seu7
        + ", mon3v3=" + mon3v3
        + ", mon5=" + mon5
        + ", monVin=" + monVin
        + ", exwdtc=" + exwdtc
        + '}';
  }
}
