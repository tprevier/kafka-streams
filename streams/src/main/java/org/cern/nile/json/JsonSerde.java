package org.cern.nile.json;

import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class JsonSerde implements Serde<JsonObject> {
  private final JsonPojoSerializer<JsonObject> serializer = new JsonPojoSerializer<>();
  private final JsonPojoDeserializer<JsonObject> deserializer = new JsonPojoDeserializer<>(JsonObject.class);

  @Override
  public void configure(Map<String, ?> configs, boolean isKey) {
    serializer.configure(configs, isKey);
    deserializer.configure(configs, isKey);
  }

  @Override
  public void close() {
    serializer.close();
    deserializer.close();
  }

  @Override
  public Serializer<JsonObject> serializer() {
    return serializer;
  }

  @Override
  public Deserializer<JsonObject> deserializer() {
    return deserializer;
  }
}
