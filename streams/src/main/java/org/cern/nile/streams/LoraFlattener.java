package org.cern.nile.streams;

import com.github.wnameless.json.flattener.JsonFlattener;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class LoraFlattener implements Streaming {
  private static final Logger LOGGER = LoggerFactory.getLogger(LoraFlattener.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;
  private Gson gson;

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    System.out.println("Configuring kafka stream...");
    final KStream<String, JsonObject> stream = sb
            .stream(configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()), Consumed.with(Serdes.String(), new JsonSerde()));

    stream
            .mapValues(this::removeDataField)
            .mapValues(this::flatten)
            .to(configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue()));

    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }

  protected JsonObject removeDataField(JsonObject value) {
    value.remove("data");
    return value;
  }

  protected Object flatten(JsonObject value) {
    return JsonFlattener.flattenAsMap(value.toString())
            .entrySet()
            .stream()
            .collect(Collectors
                    .toMap(e -> e.getKey().replace('.', '_')
                                          .replace('[', '_')
                                          .replace("]", ""), Map.Entry::getValue));
  }

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
    this.gson = new Gson();
  }
}
