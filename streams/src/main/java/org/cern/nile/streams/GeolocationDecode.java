package org.cern.nile.streams;

import com.google.gson.JsonObject;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import javax.xml.bind.DatatypeConverter;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeolocationDecode implements Streaming {
  private static final Logger LOGGER = LoggerFactory.getLogger(GeolocationDecode.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    LOGGER.info("Configuring Geolocation Decoding streams...");
    final KStream<String, JsonObject> stream = sb.stream(
        configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()),
        Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        .filter((k, v) -> v.get("data") != null && v.get("fPort") != null
            && v.get("deviceName") != null && v.get("rxInfo") != null)
        .mapValues(this::decode)
        .filter((k, v) -> v != null)
        .to(configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue()));

    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }

  private Map<String, Object> decode(JsonObject value) {
    final Map<String, Object> dict = new HashMap<>();

    decodePayload(value.get("data").getAsString(), dict);
    decorateId(value.get("deviceName").getAsString(), dict);
    try {
      parseTimestamp(value.get("rxInfo").getAsJsonArray().get(0).getAsJsonObject(), dict);
    } catch (DateTimeException | ParseException e) {
      LOGGER.error(e.getMessage());
      return null;
    }

    return dict;
  }

  private void parseTimestamp(JsonObject metadata, Map<String, Object> dict) throws DateTimeException, ParseException {
    if (metadata.get("time") == null) {
      throw new DateTimeException("Message timestamp does not contain timestamp. Ignoring message");
    }

    final String time = metadata.get("time").getAsString();
    dict.put("timestamp", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time).getTime());
  }

  private void decorateId(String devId, Map<String, Object> dict) {
    dict.put("devId", devId);
  }

  private void decodePayload(String payload, Map<String, Object> dict) {
    try {
      final byte[] decode = DatatypeConverter.parseBase64Binary(payload);
      ByteBuffer bb = ByteBuffer.wrap(decode).order(ByteOrder.LITTLE_ENDIAN);
      while (bb.hasRemaining()) {
        final int latitudeRaw = bb.getInt();
        final int longitudeRaw = bb.getInt();
        final int headingRaw = bb.get() & 0xFF;
        final int speed = bb.get() & 0xFF;
        final int voltageRaw = bb.get() & 0xFF;
        dict.put("type", "position");
        dict.put("latitudeDeg", latitudeRaw * 1e-7);
        dict.put("longitudeDeg", longitudeRaw * 1e-7);
        dict.put("inTrip", ((headingRaw & 1) != 0));
        dict.put("fixFailed", ((headingRaw & 2) != 0));
        dict.put("speedKmph", speed);
        dict.put("headingDeg", (headingRaw >> 2) * 5.625);
        dict.put("batV", voltageRaw * 0.025);
        dict.put("manDown", false);
      }
    } catch (IllegalArgumentException e) {
      LOGGER.warn(e.getMessage());
    }
  }
}
