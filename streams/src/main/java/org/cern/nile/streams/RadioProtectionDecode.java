package org.cern.nile.streams;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.enutils.Decoder;
import org.cern.nile.json.JsonSerde;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Base64;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class RadioProtectionDecode implements Streaming {

  private static final Logger LOGGER = LoggerFactory.getLogger(RadioProtectionDecode.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    System.out.println("Configuring Lora Streams Decoding");
    final KStream<String, JsonObject> stream = sb
        .stream(
            configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()),
            Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        // ignore messages not containing this parameters.
        .filter((k, v) -> v.get("fPort") != null && v.get("data") != null && v.get("rxInfo") != null && v.get("deviceName") != null)
        .mapValues(value -> {
          final String payloadRaw = value.get("data").getAsString();
          final int port = value.get("fPort").getAsInt();
          final String devId = value.get("deviceName").getAsString();
          final byte[] payloadDecoded = Base64.getDecoder().decode(payloadRaw);
          final JsonArray metadata = value.get("rxInfo").getAsJsonArray();
          final DataInputStream dis = new DataInputStream(new ByteArrayInputStream(payloadDecoded));
          try {
            return Decoder.decode(port, dis, metadata, devId);
          } catch (IOException | ParseException | RuntimeException e) {
            LOGGER.debug(e.getMessage());
            return null;
          }
        })
        .filter((k, v) -> v != null)
        .to(configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue()));

    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }
}
