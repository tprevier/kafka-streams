package org.cern.nile.streams;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.Utils;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.models.Device;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class LoraRouting implements Streaming {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoraRouting.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;
  private Collection<Device> devices;

  @Override
  public void configure(Properties configs) {
    this.configs = configs;

    try {
      final byte[] bytes = Files.readAllBytes(Paths.get(configs.getProperty(StreamConfig.RoutingProperties.ROUTING_CONFIG_PATH.getValue())));
      final Type type = new TypeToken<Collection<Device>>() {}.getType();
      this.devices = new Gson().fromJson(new String(bytes), type);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    System.out.println("Configuring kafka stream...");
    final KStream<String, JsonObject> stream = sb
        .stream(configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()), Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        .filter((key, value) -> {
          // check if the mapping exists. filter out if not.
          final JsonElement dev_id = value.get("deviceName");
          if (dev_id != null) {
            try {
              return Utils.getFirst(this.devices, dev_id.getAsString()).isPresent();
            } catch (IllegalArgumentException e) {
              LOGGER.warn(e.getMessage());
              return false;
            }
          }
          // filter out if dev_id does not exist
          return false;
        })
        .to((key, value, recordContext) -> {
          final String dev_id = value.get("deviceName").getAsString();
          try {
            return Utils.getFirst(this.devices, dev_id).get().getTopic().getName();
          } catch (IllegalArgumentException e) {
            LOGGER.warn(e.getMessage());
            return null;
          }
        });

    stream.to("mqtt-heartbeat");
    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }
}
