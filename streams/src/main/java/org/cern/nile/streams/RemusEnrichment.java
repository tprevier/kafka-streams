package org.cern.nile.streams;

import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;

public class RemusEnrichment implements Streaming {
  private KafkaStreamsClient client;
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    System.out.println("Configuring Lora Streams Decoding");
    final KStream<String, JsonObject> stream = sb
        .stream(
            configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()),
            Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        // Only ones with timestamp != null
        .filter(((key, value) -> value != null && value.get("timestamp") != null && value.get("value") != null))
        // Validate timestamps
        .filter(((key, value) -> validateTimestamp(value.get("timestamp").getAsString())))
        .to(configs.getProperty(StreamConfig.EnrichmentProperties.SINK_TOPIC.getValue()));


    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }

  private boolean validateTimestamp(String timestamp) {
    try {
      final long unix_seconds = Long.parseLong(timestamp);
      final Date date = new Date(unix_seconds);
      final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
      sdf.format(date);
      return true;
    } catch (Exception e) {
      System.out.println(e.getMessage());
      return false;
    }
  }
}
