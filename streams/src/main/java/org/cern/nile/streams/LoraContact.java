package org.cern.nile.streams;

import com.google.gson.JsonObject;
import io.kaitai.struct.ByteBufferKaitaiStream;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.tracing.LoraContactPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class LoraContact implements Streaming {
  private static final Logger LOGGER = LoggerFactory.getLogger(LoraContact.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    LOGGER.info("Configuring LoraContact Decoding streams...");
    final KStream<String, JsonObject> stream = sb.stream(
        configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()),
        Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        .filter((k, v) -> v != null && v.get("data") != null)
        .flatMapValues(value -> {
          try {
            return createLoraEvents(value.get("data").getAsString());
          } catch (RuntimeException e) {
            LOGGER.error(String.format("Could not proccess payload: %s due to:", value.get("data")), e);
            LOGGER.info(String.format("Streams state is: %s", streams.state().toString()));
            return new ArrayList<>();
          }
        })
        .filter((k, v) -> !(v instanceof List && ((List<?>) v).isEmpty()))
        .to(configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue()));

    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }

  private List<Object> createLoraEvents(String payload) throws RuntimeException {
    final LoraContactPacket loraContactPacket = decode(payload);
    final List<Object> encounters = loraContactPacket.encounters().stream().map(er -> {
      try {
        final String timezone = "Europe/Zurich";
        final HashMap<String, Object> records = new HashMap<>();
        final Instant instant = Instant.now();
        final int monthValue = instant.atZone(ZoneId.of(timezone)).getMonthValue();
        final int yearValue = instant.atZone(ZoneId.of(timezone)).getYear();
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone(timezone));
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone(timezone));
        // Month is the previous one
        // In case a message comes with the start day bigger than the current day, we know this had to be the previous month
        // E.g message day = 23 current day = 22 => message occurred last month
        if (er.startedAt().day() > instant.atOffset(ZoneOffset.UTC).getDayOfMonth()) {
          final Date parse = sdf.parse(String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue, er.startedAt().hour(),
                                                     er.startedAt().minute()));
          calendar.setTime(parse);
          calendar.add(Calendar.MONTH, -1);
        } else { // Month is the current one
          final Date parse = sdf.parse(String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue, er.startedAt().hour(),
                                                     er.startedAt().minute()));
          calendar.setTime(parse);
        }

        records.put("my_tag_id", loraContactPacket.myTagId());
        records.put("rf_tag_id", er.rfTagId());
        records.put("num_contacts", er.numContacts());
        records.put("timestamp", calendar.getTimeInMillis());
        return records;
      } catch (ParseException ex) {
        throw new RuntimeException(ex);
      }
    }).collect(Collectors.toList());
    return injectSchema(encounters);
  }

  private List<Object> injectSchema(List<Object> message) {
    final HashMap<String, Object> schema = new HashMap<>();
    final Collection<Map<String, Object>> fields = new ArrayList<>();

    fields.add(Map.of("field", "rf_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "my_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "num_contacts", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "timestamp", "type", "int64", "optional", "false", "name", "org.apache.kafka.connect.data.Timestamp"));

    schema.put("type", "struct");
    schema.put("fields", fields);

    return message.stream().map(m -> {
      final HashMap<String, Object> obj = new HashMap<>();
      obj.put("schema", schema);
      obj.put("payload", m);
      return obj;
    }).collect(Collectors.toList());
  }

  private LoraContactPacket decode(String payload) throws RuntimeException {
    final byte[] decodedBytes = Base64.getDecoder().decode(payload);
    return new LoraContactPacket(new ByteBufferKaitaiStream(decodedBytes));
  }

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }
}
