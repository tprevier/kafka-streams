package org.cern.nile.streams;

import com.google.gson.JsonObject;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.cern.nile.clients.KafkaStreamsClient;
import org.cern.nile.configs.StreamConfig;
import org.cern.nile.json.JsonSerde;
import org.cern.nile.models.ensmmrme.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class EnSmmRmeDecode implements Streaming {
  private static final Logger LOGGER = LoggerFactory.getLogger(EnSmmRmeDecode.class.getName());
  private KafkaStreams streams;
  private CountDownLatch latch;
  private Properties configs;

  @Override
  public void stream(KafkaStreamsClient kafkaStreamsClient) {
    final StreamsBuilder sb = new StreamsBuilder();
    LOGGER.info("Configuring EnSmmRmeDecode streams...");
    final KStream<String, JsonObject> stream = sb
        .stream(
            configs.getProperty(StreamConfig.ClientProperties.SOURCE_TOPIC.getValue()),
            Consumed.with(Serdes.String(), new JsonSerde()));

    stream
        .filter((k, v) -> v.get("port") != null && v.get("payload") != null)
        .mapValues(v -> decodePayload(v.get("port").getAsInt(), v.get("payload").getAsString()))
        .to(configs.getProperty(StreamConfig.DecodingProperties.SINK_TOPIC.getValue()));


    final Topology topology = sb.build();
    streams = kafkaStreamsClient.create(topology);
    latch = new CountDownLatch(1);

    // attach shutdown handler to catch control-c
    Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
      @Override
      public void run() {
        streams.close();
        latch.countDown();
      }
    });

    try {
      streams.start();
      latch.await();
    } catch (Throwable e) {
      System.exit(1);
    }
    System.exit(0);
  }

  private Payload decodePayload(int port, String payload) {
    switch (port) {
      case 0:
        try {
          return decodeHexPort0(payload);
        } catch (IOException e) {
          LOGGER.error(e.getMessage());
          return null;
        }
      case 1:
        try {
          return decodeHexPort1(payload);
        } catch (IOException e) {
          LOGGER.error(e.getMessage());
          return null;
        }
      default:
        return null;
    }
  }

  private Payload decodeHexPort1(String payload) throws IOException {
    final DataInputStream dataInputStream = payloadInputStream(payload);
    Payload toReturn = null;
    while (dataInputStream.available() > 0) {
      final int deviceId = dataInputStream.readUnsignedByte();
      final int pnum = dataInputStream.readUnsignedShort();
      final int fsens1 = new BigInteger(dataInputStream.readNBytes(3)).intValue();
      final int fref1 = new BigInteger(dataInputStream.readNBytes(3)).intValue();
      final int recharge1 = dataInputStream.readUnsignedByte();
      final int fsens2 = new BigInteger(dataInputStream.readNBytes(3)).intValue();
      final int fref2 = new BigInteger(dataInputStream.readNBytes(3)).intValue();
      final int recharge2 = dataInputStream.readUnsignedByte();
      final int seu0 = dataInputStream.readUnsignedShort();
      final int seu1 = dataInputStream.readUnsignedShort();
      final int seu2 = dataInputStream.readUnsignedShort();
      final int seu3 = dataInputStream.readUnsignedShort();
      final int seu4 = dataInputStream.readUnsignedShort();
      final int seu5 = dataInputStream.readUnsignedShort();
      final int seu6 = dataInputStream.readUnsignedShort();
      final int seu7 = dataInputStream.readUnsignedShort();
      final int mon3v3 = dataInputStream.readUnsignedShort();
      final int mon5 = dataInputStream.readUnsignedShort();
      final int monVin = dataInputStream.readUnsignedShort();
      final int exwdtc = dataInputStream.readUnsignedShort();
      toReturn = new Payload(deviceId, pnum, fsens1, fref1, recharge1, fsens2, fref2, recharge2, seu0, seu1, seu2, seu3, seu4, seu5, seu6, seu7,
                             mon3v3, mon5, monVin, exwdtc);
    }
    return toReturn;
  }

  private Payload decodeHexPort0(String payload) throws IOException {
    final DataInputStream dataInputStream = payloadInputStream(payload);
    Payload toReturn = null;
    while (dataInputStream.available() > 0) {
    }
    return null;
  }

  private DataInputStream payloadInputStream(String payload) {
    final byte[] hexPayload = DatatypeConverter.parseHexBinary(payload);
    return new DataInputStream(new ByteArrayInputStream(hexPayload));
  }

  @Override
  public void configure(Properties configs) {
    this.configs = configs;
  }
}
