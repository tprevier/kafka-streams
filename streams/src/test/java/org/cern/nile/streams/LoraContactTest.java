package org.cern.nile.streams;

import com.google.gson.Gson;
import io.kaitai.struct.ByteBufferKaitaiStream;
import org.cern.nile.tracing.LoraContactPacket;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

class LoraContactTest {
  /**
   * Test method. To be removed.
   *
   * @param args - none
   */
  public static void main(String[] args) {
    String x = "DACD1ouGAQAB+4uGDQAD+4uGAgAL+w==";
    byte[] decodedBytes = Base64.getDecoder().decode(x);
    final LoraContactPacket loraContactPacket = new LoraContactPacket(new ByteBufferKaitaiStream(decodedBytes));
    final List<Object> encounters = loraContactPacket.encounters().stream().map(er -> {
      try {
        final HashMap<String, Object> records = new HashMap<>();
        final Instant instant = Instant.now();
        final int monthValue = instant.atOffset(ZoneOffset.UTC).getMonthValue();
        final int yearValue = instant.atOffset(ZoneOffset.UTC).getYear();
        final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy'T'HH:mm");
        final Date parse = sdf.parse(String.format("%s/%s/%sT%d:%d", er.startedAt().day(), monthValue, yearValue, er.startedAt().hour(), er.startedAt().minute()));
        records.put("my_tag_id", loraContactPacket.myTagId());
        records.put("rf_tag_id", er.rfTagId());
        records.put("num_contacts", er.numContacts());
        records.put("timestamp", parse.getTime());
        return records;
      } catch (ParseException ex) {
        ex.printStackTrace();
        return null;
      }
    }).collect(Collectors.toList());
    System.out.println(encounters);

    final List<Object> encountersSchema = injectSchema(encounters);

    System.out.println(new Gson().toJson(encounters));
    System.out.println(new Gson().toJson(encountersSchema));
  }

  private static List<Object> injectSchema(List<Object> message) {
    final HashMap<String, Object> schema = new HashMap<>();
    final Collection<Map<String, Object>> fields = new ArrayList<>();

    fields.add(Map.of("field", "rf_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "my_tag_id", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "num_contacts", "type", "int64", "optional", "false"));
    fields.add(Map.of("field", "timestamp", "type", "int64", "optional", "false", "name", "org.apache.kafka.connect.data.Timestamp"));

    schema.put("type", "struct");
    schema.put("fields", fields);

    return message.stream().map(m -> {
      final HashMap<String, Object> obj = new HashMap<>();
      obj.put("schema", schema);
      obj.put("payload", m);
      return obj;
    }).collect(Collectors.toList());
  }
}
