package org.cern.nile.streams;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LoraFlattenerTest {
  // DISCLAIMER: this isn't a real payload.
  private static final String MESSAGE = "{\"applicationID\":\"2\",\"applicationName\":\"covid-distancing\","
      + "\"deviceName\":\"covid-distancing75\","
      + "\"devEUI\":\"00a9a20e000001db\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe128a6\",\"uplinkID\":\"bda6-631a-43ac-b92f-abca0556\","
      + "\"name\":\"lora-0-gw\",\"time\":\"2020-12-14T08:54:12.377063977Z\",\"rssi\":-106,\"loRaSNR\":-9,\"location\":{\"latitude\":46.23144,"
      + "\"longitude\":6.02,\"altitude\":40}},{\"gatewayID\":\"fcc276fb\",\"uplinkID\":\"e259b-8ca-4d31-800a-9e6be1\","
      + "\"name\":\"lora-7-gw\",\"time\":\"2020-12-14T08:54:12.373357386Z\",\"rssi\":-111,\"loRaSNR\":-14.8,\"location\":{\"latitude\":46.23431,"
      + "\"longitude\":6.88,\"altitude\":48}}],\"txInfo\":{\"frequency\":868100,\"dr\":2},\"adr\":false,\"fCnt\":3,\"fPort\":5,"
      + "\"data\":\"A8943JAHJmcEQBuag==\"}";

  public static void main(String[] args) {
    final LoraFlattener flattener = new LoraFlattener();
    final JsonObject msgAsJson = new JsonParser().parse(MESSAGE).getAsJsonObject();
    // Should remove the data field
    flattener.removeDataField(msgAsJson);
    System.out.println(flattener.flatten(msgAsJson));
  }
}
