package org.cern.nile.streams;

import com.google.gson.JsonParser;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;
import java.util.Map;
import org.cern.nile.enutils.RpCalibration;

public class RpCalibrationTest {
  public static void main(String[] args) {
    final String message = "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
        + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
        + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16:45:38.463126844Z\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
        + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
        + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

    final String messageParseError = "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
        + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
        + "\"name\":\"lora-0227-gw\",\"time\":\"2020-11-18T16@###:45:38.463126844Z\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
        + "\"name\":\"lora-0060-gw\",\"time\":\"2020-11-18T16:45:38.452045474Z\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
        + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";

    final String messageNullPointerTime = "{\"applicationID\":\"5\",\"applicationName\":\"cern-rp-wmon-test\",\"deviceName\":\"rp-wmon-44\","
        + "\"devEUI\":\"00e839851c156ad8\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe0f76fb\",\"uplinkID\":\"28084e96-8c1a-4255-b3b0-7f71ba815819\","
        + "\"name\":\"lora-0227-gw\",\"rssi\":-108,\"loRaSNR\":5.8,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe0f78a6\",\"uplinkID\":\"59ec4ab4-d040-458d-9700-dbfc397bcb32\","
        + "\"name\":\"lora-0060-gw\",\"rssi\":-107,\"loRaSNR\":-2.2,\"location\":{\"latitude\":0,"
        + "\"longitude\":0,\"altitude\":0}}],\"txInfo\":{\"frequency\":867100000,\"dr\":4},\"adr\":true,\"fCnt\":0,\"fPort\":8,"
        + "\"data\":\"3QABAQAAADIAAAAyAAAAAQAAAAIOEA4QDeMAAAAQASIAD/8G7goMAAI=\"}";


    Collection<Map<String, Object>> decode;
    try {
      System.out.println("TESTING CORRECT MESSAGE:");
      decode = RpCalibration.decode(new JsonParser().parse(message).getAsJsonObject());
      System.out.println(decode.toString());
      System.out.println("OUTPUT SIZE: " + decode.size());
      decode.clear();
    } catch (IOException | ParseException | NullPointerException e) {
      e.printStackTrace();
    }

    try {
      System.out.println("TESTING MESSAGE PARSE ERROR:");
      decode = RpCalibration.decode(new JsonParser().parse(messageParseError).getAsJsonObject());
      System.out.println(decode.toString());
      System.out.println("OUTPUT SIZE: " + decode.size());
      decode.clear();
    } catch (IOException | ParseException | NullPointerException e) {
      e.printStackTrace();
    }

    try {
      System.out.println("TESTING MESSAGE TIME NULL POINT ERROR:");
      decode = RpCalibration.decode(new JsonParser().parse(messageNullPointerTime).getAsJsonObject());
      System.out.println(decode.toString());
      System.out.println("OUTPUT SIZE: " + decode.size());
      decode.clear();
    } catch (IOException | ParseException | NullPointerException e) {
      e.printStackTrace();
    }
  }
}
