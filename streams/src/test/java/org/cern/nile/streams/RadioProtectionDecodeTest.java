package org.cern.nile.streams;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Base64;
import java.util.Map;
import org.cern.nile.enutils.Decoder;

public class RadioProtectionDecodeTest {
  static String message = "{\"applicationID\":\"4\",\"applicationName\":\"cern-rp-wmon\",\"deviceName\":\"rp-wmon-42\","
      + "\"devEUI\":\"002489244d4401d1\",\"rxInfo\":[{\"gatewayID\":\"fcc23dfffe20e692\",\"uplinkID\":\"033957da-fe9d-433f-b07d-fa1768d8248f\","
      + "\"name\":\"lora-0887-gw-1\",\"rssi\":-109,\"loRaSNR\":-1,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}},"
      + "{\"gatewayID\":\"fcc23dfffe0f37d8\",\"uplinkID\":\"c2b3ffe9-a8b6-4605-9654-5c66c2e95fa1\",\"name\":\"lora-0772-gw\","
      + "\"time\":\"2020-12-02T14:24:55.007536192Z\",\"rssi\":-71,\"loRaSNR\":11.2,\"location\":{\"latitude\":46.259845157587854,\"longitude\":6"
      + ".058444976806641,\"altitude\":0}},{\"gatewayID\":\"fcc23dfffe20e31f\",\"uplinkID\":\"fb5d522c-e891-4ebc-967f-96c2f38aa828\","
      + "\"name\":\"lora-0887-gw-2\",\"rssi\":-105,\"loRaSNR\":-9.8,\"location\":{\"latitude\":46.26196,\"longitude\":6.05774,\"altitude\":486}}],"
      + "\"txInfo\":{\"frequency\":868300000,\"dr\":4},\"adr\":true,\"fCnt\":734,\"fPort\":3,"
      + "\"data\":\"3QLfAAAAAQAAAAAAAAAAADwAAAAAAAAAAAAAAAAAAAAAD+oKIgeABcw=\"}";

  public static void main(String[] args) {
    final JsonObject msgAsJson = new JsonParser().parse(message).getAsJsonObject();
    final String payloadRaw = msgAsJson.get("data").getAsString();
    final int port = msgAsJson.get("fPort").getAsInt();
    final String devId = msgAsJson.get("deviceName").getAsString();
    final byte[] payloadDecoded = Base64.getDecoder().decode(payloadRaw);
    final JsonArray metadata = msgAsJson.get("rxInfo").getAsJsonArray();
    final DataInputStream dis = new DataInputStream(new ByteArrayInputStream(payloadDecoded));
    try {
      final Map decode = Decoder.decode(port, dis, metadata, devId);
      System.out.println(decode);
    } catch (IOException | ParseException e) {
      e.printStackTrace();
    }
  }
}
