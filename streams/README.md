# Nile Kafka Streams
This repository holds the code that allows us to deploy Kafka Streams applications in our machines.

## How it works
As of today this is a developing repository that allows us to deploy routing and decoding streams applications. We're trying to abstract this as much as possible so users can provide their own function easily and then the application is automatically spawned in one of our clusters.

Today, if you want any application you just need to provide a jar application that implements the <code>org.cern.nile.streams.Streaming</code> interface. We provide <code>Routing/Decoding</code> for now.

#### Configuration
Afterwards there is a configuration file <code>streams.properties</code> in which we specify which classes to use and configuration properties described below:

Common Properties (REQUIRED ALWAYS):
<ul>
<li>stream.type - the type of stream [DECODING, ROUTING]</li>
<li>stream.class - the full class name the application will use to process the messages</li>
</ul>

Client Properties (REQUIRED ALWAYS):
<ul>
<li>client.id - the client ID of the streams application seen by kafka brokers</li>
<li>source.topic - kafka topic to source from</li>
<li>kafka.cluster - kafka cluster to be used</li>
<li>truststore.location - SSL truststore location to be used for kafka authentication</li>
</ul>

Decoding Properties (REQUIRED WHEN DECODING):
<ul>
<li>sink.topic - kafka topic to sink to</li>
</ul>

Routing Properties (REQUIRED WHEN ROUTING):
<ul>
<li>routing.config.path - the path to the json containing the routing config</li>
</ul>

This file will have to be passed as argument at the application starting point. (e.g <code>java -jar application.jar &lt;path to streams.properties&gt;</code>)

On top of that there is also the need to pass JVM properties for the Kerberos configuration, them being:
<ul>
<li><code>-Djava.security.krb5.conf=&lt;insert path&gt;</code> - path to the kerberos configuration</li>
<li><code>-Djava.security.auth.login.config=&lt;insert path&gt;</code> - path to the JAAS configuration file</li>
</ul>